package userinterface;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.*;
import com.androidplot.Plot;
import com.androidplot.xy.*;
import display.DynamicPlotDataSource;
import net.bluetoothviewer.R;
import transmission.LocalProfile;

import java.text.DecimalFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: linyaoli
 * Date: 11/26/13
 * Time: 4:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class HistoryActivity extends Activity implements View.OnClickListener  {
    private XYPlot plot;
    private TextView dateDisplay;
    private TextView dateDisplay_to;
    private ImageButton pickDate;
    private ImageButton finish;
    private ImageButton showPlot;
    private Calendar calendar, calendar2;

    private MyPlotUpdater plotUpdater;
    private DynamicPlotDataSource data;
    private Cursor cursor_date;

    private class MyPlotUpdater implements Observer {
        Plot plot;
        public MyPlotUpdater(Plot plot) {
            this.plot = plot;
        }

        @Override
        public void update(Observable o, Object arg) {
            try {
                plot.redraw();
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private Button slideButton;
    private SlidingDrawer slidingDrawer;
    private ImageButton valueButton;

    private Number[] displayValuesNumber = null;

    /**
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        Button b = (Button) v;
        Toast.makeText(HistoryActivity.this,
                b.getText() + " is Clicked :)", Toast.LENGTH_SHORT).show();
    }

    /**
     *
     * @param savedInstanceState
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history);
        initialDatabase();

        //////
        dateDisplay = (TextView) findViewById(R.id.textView_date);
        dateDisplay_to = (TextView) findViewById(R.id.textView_todate);

        pickDate = (ImageButton) findViewById(R.id.imageButton_date);
        finish = (ImageButton) findViewById(R.id.imageButton_return);
        showPlot = (ImageButton) findViewById(R.id.imageButton_refresh);
        valueButton = (ImageButton) findViewById(R.id.imageButton_value);

        //Drawer
        slideButton = (Button) findViewById(R.id.slideButton);
        slidingDrawer = (SlidingDrawer) findViewById(R.id.SlidingDrawer);

        slidingDrawer.setOnDrawerOpenListener(new SlidingDrawer.OnDrawerOpenListener() {
            @Override
            public void onDrawerOpened() {
                slideButton.setText(" down ");
            }
        });
        slidingDrawer.setOnDrawerCloseListener(new SlidingDrawer.OnDrawerCloseListener() {
            @Override
            public void onDrawerClosed() {
                slideButton.setText(" up ");
            }
        });

        calendar = Calendar.getInstance();
        calendar2 = Calendar.getInstance();
//        dateDisplay.setText( calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US) + "-" +
//                Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + "-" +
//                Integer.toString(calendar.get(Calendar.YEAR)) + "|" + calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US));
        //dateDisplay.setText("Choose  date ");

        pickDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                DatePickerDialog dpd_from = new DatePickerDialog(HistoryActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                  int dayOfMonth) {
                                calendar.set(year, monthOfYear, dayOfMonth);
                                dateDisplay.setText( calendar.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US) + "-" +
                                        Integer.toString(dayOfMonth) + "-" +
                                        Integer.toString(year) + "|" + calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US) );

                            }
                        },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH));
                dpd_from.setTitle("Date From");


                DatePickerDialog dpd_to = new DatePickerDialog(HistoryActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                  int dayOfMonth) {
                                calendar2.set(year, monthOfYear, dayOfMonth);
                                dateDisplay_to.setText( calendar2.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.US) + "-" +
                                        Integer.toString(dayOfMonth) + "-" +
                                        Integer.toString(year) + "|" + calendar2.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US) );

                            }
                        },calendar2.get(Calendar.YEAR),calendar2.get(Calendar.MONTH),calendar2.get(Calendar.DAY_OF_MONTH));

                dpd_to.setTitle("Date to");
                dpd_to.show();
                dpd_from.show();

            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                finish();
            }
        });
        final ContextThemeWrapper cw = new ContextThemeWrapper( this, R.style.valueTheme );

        valueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //To change body of implemented methods use File | Settings | File Templates.
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(
                        cw);

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                        HistoryActivity.this,
                        android.R.layout.simple_list_item_1);

                if (cursor != null && cursor_date != null) {
                    cursor.moveToFirst();
                    cursor_date.moveToFirst();
                    int count = cursor.getCount();

                    for (int i=0; i<count;i++) {
                        arrayAdapter.add(cursor_date.getString(0) + " : " + cursor.getString(0));
                        cursor.moveToNext();
                        cursor_date.moveToNext();
                    }

//                builderSingle.setNegativeButton("cancel",
//                        new DialogInterface.OnClickListener() {
//
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                //dialog.dismiss();
//                            }
//                        });
                builderSingle.setAdapter(arrayAdapter, null);
                builderSingle.show();

//                builderSingle.setAdapter(arrayAdapter,
//                        new DialogInterface.OnClickListener() {
//
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//
////                                String strName = arrayAdapter.getItem(which);
////                                AlertDialog.Builder builderInner = new AlertDialog.Builder(
////                                        HistoryActivity.this);
////                                builderInner.setMessage(strName);
////                                builderInner.setTitle("Your Selected Item is");
////                                builderInner.setPositiveButton("Ok",
////                                        new DialogInterface.OnClickListener() {
////
////                                            @Override
////                                            public void onClick(
////                                                    DialogInterface dialog,
////                                                    int which) {
////                                                dialog.dismiss();
////                                            }
////                                        });
////                                builderInner.show();
//                            }
//                        });
//                builderSingle.show();
                }
            }
        });
        plot = (XYPlot) findViewById(R.id.view);

        plot.setBorderStyle(Plot.BorderStyle.ROUNDED, (float)8, (float)8);

        //plot.setBorderStyle(Plot.BorderStyle.NONE, null, null);
        //plot.setPlotMargins(0, 0, 0, 0);
        //plot.setPlotPadding(0, 0, 0, 0);
        //plot.setGridPadding(0, 10, 5, 0);

        plot.setBackgroundColor(Color.WHITE);
        plot.getGraphWidget().getBackgroundPaint().setColor(Color.WHITE);
        plot.getGraphWidget().getGridBackgroundPaint().setColor(Color.WHITE);

        plot.getGraphWidget().getDomainLabelPaint().setColor(Color.BLACK);
        plot.getGraphWidget().getRangeLabelPaint().setColor(Color.BLACK);

        plot.getGraphWidget().getDomainOriginLabelPaint().setColor(Color.BLACK);
        plot.getGraphWidget().getDomainOriginLinePaint().setColor(Color.BLACK);
        plot.getGraphWidget().getRangeOriginLinePaint().setColor(Color.BLACK);

        plot.setDomainLabel("Date");
        plot.setRangeLabel("Concentration(ppb)");

        // Domain
        plot.setDomainStepMode(XYStepMode.SUBDIVIDE);
        plot.setDomainValueFormat(new DecimalFormat("0"));
        plot.setDomainBoundaries(0, 10, BoundaryMode.FIXED);
        plot.setDomainStepValue(2);

        //Range
        plot.setRangeBoundaries(0, 50000, BoundaryMode.FIXED);
        plot.setRangeStepValue(11);
        plot.setRangeValueFormat(new DecimalFormat("0"));

        //Remove legend
        plot.getLayoutManager().remove(plot.getLegendWidget());

        //plot.getLayoutManager().remove(plot.getDomainLabelWidget());
        //plot.getLayoutManager().remove(plot.getRangeLabelWidget());
        //plot.getLayoutManager().remove(plot.getTitleWidget());


        ////////////////////////////////////////////////////////////
        // Update Plot
        //plotUpdater = new MyPlotUpdater(plot);

        showPlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                doQuery(calendar, calendar2);
                plot.clear();
                /*
            browse cursor to static array.
         */
                int count = cursor.getCount();
                Number[] values = new Number[count];

                for (int i=0; i<count;i++) {
                    cursor.moveToNext();
                    values[i] = Integer.parseInt(cursor.getString(0));
                }

                //displayValuesNumber = values;

                XYSeries series = new SimpleXYSeries(
                        Arrays.asList(values),
                        SimpleXYSeries.ArrayFormat.Y_VALS_ONLY,
                        null
                );

                LineAndPointFormatter seriesFormatter = new LineAndPointFormatter(
                        Color.GRAY,
                        Color.RED, null,
                        new PointLabelFormatter(Color.BLUE, 25, 15));
               // seriesFormatter.setPointLabelFormatter(new PointLabelFormatter());
                Paint lineFill1 = new Paint();
                lineFill1.setAlpha(200);
                lineFill1.setShader(new LinearGradient(0, 0, 0, 250, Color.WHITE, Color.GRAY, Shader.TileMode.MIRROR));
                seriesFormatter.setFillPaint(lineFill1);
//                seriesFormatter.configure(getApplicationContext(),
//                        R.xml)
                plot.setDomainBoundaries(0, count+1, BoundaryMode.FIXED);
                plot.setDomainStepValue(6);
                plot.setRangeBoundaries(0, findTheLargest(values)*1.5, BoundaryMode.FIXED);
                plot.setRangeStepValue(11);

                plot.addSeries(series, seriesFormatter);
                if (count != 0) {
                    plot.redraw();
                }
            }
        });
    }

    /**
     *
     * @param num
     * @return
     */
    private int findTheLargest(Number[] num) {

        int larger=0;
        for (int i=0;i<num.length;i++) {
            if (num[i].intValue() > larger)
                larger = num[i].intValue();
        }
        return larger;
    }

    private static LocalProfile mDb = null;
    private static SQLiteDatabase sqlite;
    private Cursor cursor;

    /**
     *
     * @param cal_from
     * @param cal_to
     */
    private void doQuery(Calendar cal_from, Calendar cal_to) {
        String date = "_DATE";
        String value1 = "_VALUE1";

        String sql_select =
                "SELECT ";
        String sql_from = " FROM CONC WHERE _DATE BETWEEN " +
                        // date from
                        "'" + Integer.toString(cal_from.get(Calendar.YEAR)) +
                        "-" + String.format( "%1$02d" , cal_from.get(Calendar.MONTH)+1) +
                        "-" + String.format("%1$02d", cal_from.get(Calendar.DAY_OF_MONTH)) +
                        "'" +
                        " AND " +
                        //date to
                        "'" + Integer.toString(cal_to.get(Calendar.YEAR)) +
                        "-" + String.format("%1$02d", cal_to.get(Calendar.MONTH) + 1) +
                        "-" + String.format("%1$02d", cal_to.get(Calendar.DAY_OF_MONTH) + 1) +
                        "'"
                ;
        try {

            cursor = sqlite.rawQuery ( sql_select+value1+sql_from,
                    null
            );

            cursor_date = sqlite.rawQuery(sql_select+date+sql_from,
                    null
            );

        }
        catch (Exception e) {
            e.printStackTrace();
        }
//
//        while (cursor.moveToNext()) {
//            String a = cursor.getString(0);
//        }

    }

    /**
     *
     */
    private void initialDatabase() {
        try {
            mDb = new LocalProfile(this);
            sqlite = mDb.getWritableDatabase();
            //mDb.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     */
    @Override
    protected void onDestroy() {
        //cursor.close();
        mDb.close();
        super.onDestroy();    //To change body of overridden methods use File | Settings | File Templates.
    }

}