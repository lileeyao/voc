/*
 * Copyright (C) 2010 Janos Gyerik
 *
 * This file is part of BluetoothViewer.
 *
 * BluetoothViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BluetoothViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BluetoothViewer.  If not, see <http://www.gnu.org/licenses/>.
 * -----------------------------------------------------------------------------------------
 * |Copyright (C) 2014 Linyao Li                                                           |
 * |                                                                                       |
 * |Project: Sensor Monitor.                                                               |
 * |                                                                                       |
 * |The sensor monitor project inherits from the bluetoothviewer core, using its basic     |
 * |function to connect with bluetooth device and sending and receiving data. Its code     |
 * |has been deeply modified.                                                              |
 * |                                                                                       |
 * |The sensor monitor project strictly follows the GNU General Public License, will only  |
 * |be used in research activity.                                                          |
 * -----------------------------------------------------------------------------------------
 */

package userinterface;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.*;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.*;
import com.androidplot.Plot;
import com.androidplot.xy.*;
import display.DynamicPlotDataSource;
import display.DynamicPlotSeries;
import net.bluetoothviewer.R;
import processing.SensorCurve;
import transmission.BluetoothChatService;
import transmission.LocalProfile;

import java.text.DecimalFormat;
import java.util.*;

/**
 * This is the main Activity that displays the current session.
 */
@SuppressLint("SimpleDateFormat")
public class BluetoothViewer extends Activity {
    // Debugging
    private static final String TAG = "EnviroSensor";
    private static final boolean D = true;

    // Message types sent from the BluetoothService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_READ_SINGLE_BYTE = 6;

    // Key names received from the BluetoothService Handler
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    // Intent request codes
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;

    // Layout Views
    private TextView mTitle;
    private EditText mOutEditText;
    private View mSendTextContainer;
    //private TextView mTextviewConc;
    private Chronometer mChronometer;
    private TextView mTextStatus;

    // Toolbar
    private ImageButton mToolbarConnectButton;
    private ImageButton mToolbarDisconnectButton;
    private ImageButton mToolbarPauseButton;
    private ImageButton mToolbarPlayButton;
    private ImageButton mToolbarSingletonButton;
    private ImageButton mToolbarHomeButton;

    private LineAndPointFormatter series2Format;

    // Name of the connected device
    private String mConnectedDeviceName = null;
    // Array adapter for the conversation thread
    private ArrayAdapter<String> mConversationArrayAdapter;
    // String buffer for outgoing messages
    private StringBuffer mOutStringBuffer;
    // Local Bluetooth adapter
    private BluetoothAdapter mBluetoothAdapter = null;
    // Member object for the Bluetooth services
    private BluetoothChatService mBluetoothService = null;

    // State variables
    private boolean paused = false;
    private boolean connected = false;

    // DB helper
    private LocalProfile db = null;

     //Queue buffer
    private Queue<Byte> msgQueueBuffer = new LinkedList<Byte>();
    private Queue<Character> dateQueueBuffer = new LinkedList<Character>();
    //Sensor curve
    private static SensorCurve mSensorCurve;
    private static char mConcentration = 0;

    //Plot

    /**
     *
     */
    private class MyPlotUpdater implements Observer {
        Plot plot;
        private SensorCurve mSensorCurve;
        private boolean isPurging = false;
        public MyPlotUpdater(Plot plot, SensorCurve mSensorCurve) {
            this.mSensorCurve = mSensorCurve;
            this.plot = plot;
        }

        @Override
        public void update(Observable o, Object arg) {
            try {
                /*
                this.isPurging = mSensorCurve.getPurgingStatus();
                   */
                if (mSensorCurve.getLargestConcentration()>0) {
                    series2Format.setPointLabelFormatter(new PointLabelFormatter(Color.BLUE, 25, 15));
                    concentrationPlot.setRangeBoundaries(0,
                            (int)mSensorCurve.getLargestConcentration()*1.3,
                            BoundaryMode.FIXED);

                }
                if (mSensorCurve.getLargestRaw() > 0) {
                    int diff = (int)((mSensorCurve.getLargestRaw() - mSensorCurve.getSmallestRaw())*0.8);
                    dynamicPlot.setRangeBoundaries((int)mSensorCurve.getSmallestRaw() - diff,
                            (int)mSensorCurve.getLargestRaw() + diff,
                            BoundaryMode.FIXED);
                }

                plot.redraw();
            }catch ( Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static XYPlot dynamicPlot;
    private static XYPlot concentrationPlot;
    private MyPlotUpdater plotUpdater;
    private MyPlotUpdater plotUpdater_ppb;
    private static DynamicPlotDataSource data;
    private static DynamicPlotDataSource data_ppb;

    /*
    update textview.
 */
    /**
     *
     */
    private final Runnable mUpdateUITimerTask = new Runnable() {
        @Override
        public void run() {
            //To change body of implemented methods use File | Settings | File Templates.
            //mTextviewConc.setText(Integer.toString((int)mConcentration));
            Log.d(TAG, "+ UPDATE CONCENTRATION +" + Integer.toString((int)mConcentration));
        }
    };

    private final Handler mTextviewHandler = new Handler();

    /**
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        // Set up the window layout
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.main);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_title);

        // Set up the custom title
        mTitle = (TextView) findViewById(R.id.title_left_text);
        mTitle.setText(R.string.app_name);
        mTitle = (TextView) findViewById(R.id.title_right_text);
       // mTextviewConc = (TextView) findViewById(R.id.textView_concentration);
        mChronometer = (Chronometer) findViewById(R.id.chronometer);
        mTextStatus = (TextView) findViewById(R.id.textView1);

        mSendTextContainer = findViewById(R.id.send_text_container);
        //////////////////////////////////////////////////////
        dynamicPlot = (XYPlot) findViewById(R.id.plot);
        concentrationPlot = (XYPlot) findViewById(R.id.view1);

        dateQueueBuffer.offer((char)0);
        //////////////////////////////////////////////////////////////
        ////////// Database helper
        db = new LocalProfile(this);
        /////raw reading Plot setup ////
        ////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////
        ////////// Plotting ////////////////////
        //dynamicPlot.setBorderStyle(Plot.BorderStyle.ROUNDED, (float)8, (float)8);
//        dynamicPlot.setBorderStyle(Plot.BorderStyle.NONE, null, null);
//        dynamicPlot.setPlotMargins(0, 0, 0, 0);
//        dynamicPlot.setPlotPadding(0, 0, 0, 0);
//        dynamicPlot.setGridPadding(0, 10, 5, 0);

        dynamicPlot.setBackgroundColor(Color.WHITE);
        dynamicPlot.getGraphWidget().getBackgroundPaint().setColor(Color.WHITE);
        dynamicPlot.getGraphWidget().getGridBackgroundPaint().setColor(Color.WHITE);

        dynamicPlot.getGraphWidget().getDomainLabelPaint().setColor(Color.BLACK);
        dynamicPlot.getGraphWidget().getRangeLabelPaint().setColor(Color.BLACK);

        dynamicPlot.getGraphWidget().getDomainOriginLabelPaint().setColor(Color.BLACK);
        dynamicPlot.getGraphWidget().getDomainOriginLinePaint().setColor(Color.BLACK);
        dynamicPlot.getGraphWidget().getRangeOriginLinePaint().setColor(Color.BLACK);

        // Domain
        dynamicPlot.setDomainStepMode(XYStepMode.SUBDIVIDE);
        dynamicPlot.setDomainValueFormat(new DecimalFormat("0"));
        dynamicPlot.setDomainBoundaries(0, 200, BoundaryMode.FIXED);
        dynamicPlot.setDomainStepValue(11);
        dynamicPlot.setDomainLabel("timeline");


        //Range                                      //MARK
        dynamicPlot.setRangeBoundaries(20000, 60000, BoundaryMode.FIXED);
        dynamicPlot.setRangeStepValue(5);
        dynamicPlot.setRangeValueFormat(new DecimalFormat("0"));
        dynamicPlot.setRangeLabel("raw sensor reading(s)");

        //Remove legend
        dynamicPlot.getLayoutManager().remove(dynamicPlot.getLegendWidget());
        //dynamicPlot.getLayoutManager().remove(dynamicPlot.getDomainLabelWidget());
        //dynamicPlot.getLayoutManager().remove(dynamicPlot.getRangeLabelWidget());
        //dynamicPlot.getLayoutManager().remove(dynamicPlot.getTitleWidget());


        mSensorCurve = new SensorCurve(dateQueueBuffer, dynamicPlot, db);

        data = new DynamicPlotDataSource(mSensorCurve, "raw");
        data_ppb = new DynamicPlotDataSource(mSensorCurve, "ppb");

        plotUpdater = new MyPlotUpdater(dynamicPlot, mSensorCurve);
        plotUpdater_ppb = new MyPlotUpdater(concentrationPlot, mSensorCurve);

        data.addObserver(plotUpdater);
        data_ppb.addObserver(plotUpdater_ppb);
         // Turn the above arrays into XYSeries':
        DynamicPlotSeries sine1Series = new DynamicPlotSeries(data, 0, null);
        LineAndPointFormatter series1Format = new LineAndPointFormatter(Color.GRAY, null, null, new PointLabelFormatter(Color.TRANSPARENT));

        dynamicPlot.addSeries(sine1Series, series1Format);

//        dynamicPlot.getGraphWidget().setSize(new SizeMetrics(
//                0, SizeLayoutType.FILL,
//                0, SizeLayoutType.FILL));

        // Create a formatter to use for drawing a series using LineAndPointRenderer:

        // setup our line fill paint to be a slightly transparent gradient:
        Paint lineFill = new Paint();
        lineFill.setAlpha(200);
        lineFill.setShader(new LinearGradient(0, 0, 0, 250, Color.WHITE, Color.GRAY, Shader.TileMode.MIRROR));
        series1Format.setFillPaint(lineFill);

        // add a new series' to the xyplot:
        dynamicPlot.addSeries(sine1Series, series1Format);
        // by default, AndroidPlot displays developer guides to aid in laying out your plot.
        // To get rid of them call disableAllMarkup():
        dynamicPlot.setMarkupEnabled(false);
        //////////////////////////////////
        // This is a copycat !!
        ///////////
        {
            //dynamicPlot.setBorderStyle(Plot.BorderStyle.ROUNDED, (float)8, (float)8);
//            concentrationPlot.setBorderStyle(Plot.BorderStyle.NONE, null, null);
//            concentrationPlot.setPlotMargins(0, 0, 0, 0);
//            concentrationPlot.setPlotPadding(0, 0, 0, 0);
//            concentrationPlot.setGridPadding(0, 10, 5, 0);
            concentrationPlot.setBackgroundColor(Color.WHITE);
            concentrationPlot.getGraphWidget().getBackgroundPaint().setColor(Color.WHITE);
            concentrationPlot.getGraphWidget().getGridBackgroundPaint().setColor(Color.WHITE);

            concentrationPlot.getGraphWidget().getDomainLabelPaint().setColor(Color.BLACK);
            concentrationPlot.getGraphWidget().getRangeLabelPaint().setColor(Color.BLACK);

            concentrationPlot.getGraphWidget().getDomainOriginLabelPaint().setColor(Color.BLACK);
            concentrationPlot.getGraphWidget().getDomainOriginLinePaint().setColor(Color.BLACK);
            concentrationPlot.getGraphWidget().getRangeOriginLinePaint().setColor(Color.BLACK);

            // Domain
            concentrationPlot.setDomainStepMode(XYStepMode.SUBDIVIDE);
            concentrationPlot.setDomainValueFormat(new DecimalFormat("0"));
            concentrationPlot.setDomainBoundaries(-1, 10, BoundaryMode.FIXED);
            concentrationPlot.setDomainStepValue(6);

            //Range
            concentrationPlot.setRangeBoundaries(0, 50000, BoundaryMode.FIXED);
            concentrationPlot.setRangeStepValue(5);
            //mySimpleXYPlot.setRangeStep(XYStepMode.SUBDIVIDE, values.length);
            concentrationPlot.setRangeValueFormat(new DecimalFormat("0"));

            //Remove legend
            concentrationPlot.getLayoutManager().remove(concentrationPlot.getLegendWidget());
//            concentrationPlot.getLayoutManager().remove(concentrationPlot.getDomainLabelWidget());
//            concentrationPlot.getLayoutManager().remove(concentrationPlot.getRangeLabelWidget());
//            concentrationPlot.getLayoutManager().remove(concentrationPlot.getTitleWidget());
            concentrationPlot.setRangeLabel("concentration(ppb)");
            concentrationPlot.setDomainLabel("count(s)");

            // Turn the above arrays into XYSeries':
            DynamicPlotSeries sine2Series = new DynamicPlotSeries(data_ppb, 0, "Concentration");
            //concentrationPlotSeries sine2Series = new concentrationPlotSeries(data, 1, "Sine 2");
            //series2Format = new LineAndPointFormatter(Color.GRAY, Color.RED, null, new PointLabelFormatter(Color.BLUE));
            series2Format = new LineAndPointFormatter(Color.GRAY, Color.RED, null, null);
            concentrationPlot.addSeries(sine2Series, series2Format);
//
//            concentrationPlot.getGraphWidget().setSize(new SizeMetrics(
//                    0, SizeLayoutType.FILL,
//                    0, SizeLayoutType.FILL));

            // Create a formatter to use for drawing a series using LineAndPointRenderer:

            // setup our line fill paint to be a slightly transparent gradient:
            Paint lineFill1 = new Paint();
            lineFill1.setAlpha(200);
            lineFill1.setShader(new LinearGradient(0, 0, 0, 250, Color.WHITE, Color.GRAY, Shader.TileMode.MIRROR));
            series2Format.setFillPaint(lineFill1);
            // add a new series' to the xyplot:
            concentrationPlot.addSeries(sine2Series, series2Format);

            // by default, AndroidPlot displays developer guides to aid in laying out your plot.
            // To get rid of them call disableAllMarkup():
            concentrationPlot.setMarkupEnabled(false);
        }
        ////////// Plotting ends here.
        //////////////////////////////////////////////////////////////////////

        mToolbarConnectButton = (ImageButton) findViewById(R.id.toolbar_btn_connect);
        mToolbarConnectButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                startDeviceListActivity();
            }
        });

        mToolbarDisconnectButton = (ImageButton) findViewById(R.id.toolbar_btn_disconnect);
        mToolbarDisconnectButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                new AlertDialog.Builder(BluetoothViewer.this)
                        .setTitle("Disconnect")
                        .setMessage("Are you sure to disconnect the Bluetooth transmission?")
                        .setNegativeButton("Cancel", null)
                        .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                disconnectDevices();
                            }
                        }).show();

            }
        });

        mToolbarPauseButton = (ImageButton) findViewById(R.id.toolbar_btn_pause);
        mToolbarPauseButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                paused = true;
                onPausedStateChanged();
            }
        });

        mToolbarSingletonButton = (ImageButton) findViewById(R.id.toolbar_btn_start_singleton);
        mToolbarSingletonButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                sendMessage("New Cycle");
            }
        });

        mToolbarPlayButton = (ImageButton) findViewById(R.id.toolbar_btn_play);
        mToolbarPlayButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                paused = false;
                onPausedStateChanged();
            }
        });
        // Get local Bluetooth adapter
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        mToolbarHomeButton = (ImageButton) findViewById(R.id.toolbar_btn_home);
        mToolbarHomeButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                new AlertDialog.Builder(BluetoothViewer.this)
                        .setTitle("Exit")
                        .setMessage("Are you sure to exit?")
                        .setNegativeButton("Cancel", null)
                        .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mBluetoothAdapter.disable();
                                Intent intent_main = new Intent(getBaseContext(),
                                        Welcome.class);
                                startActivityForResult(intent_main, 0);
                            }
                        }).show();

            }
        });


        Bundle bData = this.getIntent().getExtras();
        ((TextView)findViewById(R.id.textView_scenario)).setText(bData.getString("scenario"));
        ((TextView)findViewById(R.id.textView_mode)).setText(bData.getString("mode"));

        mSensorCurve.setScenarioAndMode(bData.getString("scenario"), bData.getString("mode"));

        ////////////////////////
        /**
         * Individual thread to hold refreshing two data plots.
         */
        new Thread(data).start();
        new Thread(data_ppb).start();

    }

    /**
     *  Call for device list while connecting device is needed.
     */
    private void startDeviceListActivity() {
        Intent serverIntent = new Intent(this, DeviceListActivity.class);
        startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
    }

    /**
     *
     */
    @Override
    public void onStart() {
        super.onStart();
        if (D) Log.e(TAG, "++ ON START ++");

        // If BT is not on, request that it be enabled.
        // setupUserInterface() will then be called during onActivityResult
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the Bluetooth session
        } else {
            if (mBluetoothService == null) setupUserInterface();
        }
    }

    /**
     *
     */
    @Override
    public synchronized void onResume() {
        super.onResume();
        if (D) Log.e(TAG, "+ ON RESUME +");
        //mTextviewHandler.post(mUpdateUITimerTask);
    }

    /**
     *
     */
    private void setupUserInterface() {
        Log.d(TAG, "setupUserInterface()");

        // Initialize the array adapter for the conversation thread
        //mConversationArrayAdapter = new ArrayAdapter<String>(this, R.layout.mylist);
        //ListView mConversationView = (ListView) findViewById(R.id.in);
        //mConversationView.setAdapter(mConversationArrayAdapter);

        //mConversationArrayAdapter.add("Greetings ! ");

        // Initialize the compose field with a listener for the return key
        mOutEditText = (EditText) findViewById(R.id.edit_text_out);
        mOutEditText.setOnEditorActionListener(mWriteListener);

        // Initialize the send button with a listener that for click events
        Button mSendButton = (Button) findViewById(R.id.button_send);
        mSendButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // Send a message using content of the edit text widget
                TextView view = (TextView) findViewById(R.id.edit_text_out);
                String message = view.getText().toString();
                sendMessage(message);
            }
        });

        // Initialize the BluetoothService to perform Bluetooth connections
        mBluetoothService = new BluetoothChatService(mHandler);

        // Initialize the buffer for outgoing messages
        mOutStringBuffer = new StringBuffer("");

        onBluetoothStateChanged();
    }

    /**
     *
     */
    @Override
    public synchronized void onPause() {
        super.onPause();
        if (D) Log.e(TAG, "- ON PAUSE -");
        //mTextviewHandler.removeCallbacks(mUpdateUITimerTask);
    }

    /**
     *
     */
    @Override
    public void onStop() {
        super.onStop();
        if (D) Log.e(TAG, "-- ON STOP --");
        //mTextviewHandler.removeCallbacks(mUpdateUITimerTask);
    }

    /**
     *
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        // Stop the Bluetooth services
        if (mBluetoothService != null) mBluetoothService.stop();
        mTextviewHandler.removeCallbacks(mUpdateUITimerTask);
        if (D) Log.e(TAG, "--- ON DESTROY ---");
        db.close();
    }

    /**
     * Sends a message.
     *
     * @param message A string of text to send.
     */
    private void sendMessage(String message) {
        // Check that we're actually connected before trying anything
        if (mBluetoothService.getState() != BluetoothChatService.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }


        // Check that there's actually something to send
        if (message.length() > 0) {
            //message += "\n";
            // Get the message bytes and tell the BluetoothService to write
            byte[] send;
            if (message.equals("New Cycle")){
                send = new byte[1];
                send[0] = (byte)22;
                Log.d(TAG, "writing -> " + Byte.toString(send[0]));
                Toast.makeText(this, "Request a new cycle - singleton mode", Toast.LENGTH_SHORT).show();
            }
            else
                send = message.getBytes();
            mBluetoothService.write(send);
            // Reset out string buffer to zero and clear the edit text field
            mOutStringBuffer.setLength(0);
            mOutEditText.setText(mOutStringBuffer);
        }
    }

    // The action listener for the EditText widget, to listen for the return key
    private TextView.OnEditorActionListener mWriteListener =
            new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                    // If the action is a key-up event on the return key, send the message
                    if (actionId == EditorInfo.IME_NULL && event.getAction() == KeyEvent.ACTION_UP) {
                        String message = view.getText().toString();
                        sendMessage(message);
                    }
                    if (D) Log.i(TAG, "END onEditorAction");
                    return true;
                }
            };

    // The Handler that gets information back from the BluetoothService
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    if (D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    switch (msg.arg1) {
                        case BluetoothChatService.STATE_CONNECTED:
                            connected = true;
                            mTitle.setText(mConnectedDeviceName);
                            break;
                        case BluetoothChatService.STATE_CONNECTING:
                            mTitle.setText(R.string.title_connecting);
                            break;
                        case BluetoothChatService.STATE_NONE:
                            connected = false;
                            mTitle.setText(R.string.title_not_connected);
                            break;
                    }
                    onBluetoothStateChanged();
                    break;
                case MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);

                    break;
                case MESSAGE_READ_SINGLE_BYTE:
                    break;
                case MESSAGE_READ:
                    if (paused) break;
                    //  String bt = (String)msg.obj;
                    byte[] bt = (byte[]) msg.obj;

                    for (int i = 0; i < 20; i++) {
                        //    Log.d(TAG, Byte.toString(bt[i]));
                        msgQueueBuffer.offer(bt[i]);
                    }
                    try {
                        mSensorCurve.update(msgQueueBuffer);
                        if (mSensorCurve.getPurgingStatus()) {
                            mConcentration = mSensorCurve.getConcentration();
                            mTextviewHandler.post(mUpdateUITimerTask);
                            mTextStatus.setText("Sampling");
                            Log.d(TAG, Boolean.toString(mSensorCurve.getPurgingStatus()));
                        }
                        else
                            mTextStatus.setText("Purging");
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Toast.makeText(getApplicationContext(), "Connected to "
                            + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_TOAST:
                    Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST),
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    /**
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (D) Log.d(TAG, "onActivityResult " + resultCode);
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    // Get the device MAC address
                    String address = data.getExtras()
                            .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    // Get the BLuetoothDevice object
                    BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                    // Attempt to connect to the device
                    mBluetoothService.connect(device);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode != Activity.RESULT_OK) {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "BT not enabled");
                    Toast.makeText(this, "Bluetooth not enabled", Toast.LENGTH_SHORT).show();
                }
                setupUserInterface();
        }
    }

    /**
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // MenuInflater inflater = getMenuInflater();
        // inflater.inflate(R.menu.main, menu);

        return true;
    }

    /**
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	/*
        switch (item.getItemId()) {
            case R.id.menu_github:
                openURL(getString(R.string.url_github));
                break;
            case R.id.menu_rate:
                openURL(getString(R.string.url_rate));
                break;
            case R.id.menu_buy:
                openURL(getString(R.string.url_full_app));
                break;
        }*/
        return false;
    }

    /**
     *
     * @param url
     */
    private void openURL(String url) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
    }

    /**
     *
     */
    private void disconnectDevices() {
        if (mBluetoothService != null) mBluetoothService.stop();
        try {
            mSensorCurve.getWriter().close();
            mSensorCurve.getWriter_ppb().close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        onBluetoothStateChanged();
    }

    /**
     *
     */
    private void onBluetoothStateChanged() {
        if (connected) {
            mChronometer.setBase(SystemClock.elapsedRealtime());
            mChronometer.start();
            mToolbarConnectButton.setVisibility(View.GONE);
            mToolbarDisconnectButton.setVisibility(View.VISIBLE);
            mToolbarSingletonButton.setVisibility(View.VISIBLE);
            mToolbarHomeButton.setVisibility(View.VISIBLE);
            // start handler 5 seconds after onCreate().
            mTextviewHandler.postDelayed(mUpdateUITimerTask, 20);
            //  mSendTextContainer.setVisibility(View.VISIBLE);
        } else {
            mChronometer.stop();
            mToolbarSingletonButton.setVisibility(View.GONE);
            mToolbarConnectButton.setVisibility(View.VISIBLE);
            mToolbarDisconnectButton.setVisibility(View.GONE);
            mToolbarHomeButton.setVisibility(View.VISIBLE);
            //  mSendTextContainer.setVisibility(View.GONE);
        }
        paused = false;
        onPausedStateChanged();
    }

    /**
     *
     */
    private void onPausedStateChanged() {
        if (connected) {
            if (paused) {
                Toast.makeText(this, "Bluetooth is paused", Toast.LENGTH_SHORT).show();
                mToolbarPlayButton.setVisibility(View.VISIBLE);
                mToolbarPauseButton.setVisibility(View.GONE);
            } else {
                mToolbarPlayButton.setVisibility(View.GONE);
                mToolbarPauseButton.setVisibility(View.VISIBLE);
            }
        } else {
            mToolbarPlayButton.setVisibility(View.GONE);
            mToolbarPauseButton.setVisibility(View.GONE);
        }
    }

    /**
     *
     */
    private class MyAdapter implements SpinnerAdapter {

        ArrayList<String> data;

        public MyAdapter(ArrayList<String> data) {
            this.data = data;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver dataSetObserver) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position ;
        }

        @Override
        public int getItemViewType (int position) {
            return android.R.layout.simple_spinner_dropdown_item;
        }

        @Override
        public View getView (int position, View convertView, ViewGroup parent) {
            TextView v = new TextView(getApplicationContext());
            v.setTextColor(Color.BLACK);
            v.setBackgroundColor(Color.WHITE);
            v.setText(data.get(position));

            return v;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean isEmpty() {
            return false;  //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }


        @Override
        public View getDropDownView(int i, View view, ViewGroup viewGroup) {
            return this.getView(i, view, viewGroup);  //To change body of implemented methods use File | Settings | File Templates.
        }
    }
}