package userinterface;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import net.bluetoothviewer.R;
import transmission.thirdlib.capture.CaptureActivity;

/**
 * Created with IntelliJ IDEA.
 * User: linyaoli
 * Date: 11/26/13
 * Time: 2:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class GuideActivity extends Activity {

    private static CheckBox checkBoxCartridge ;
    private static CheckBox checkBoxQR ;
    private static CheckBox checkBoxConfig ;
    private static Button buttonStart;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //if (requestCode == 0)
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guide);
        checkBoxCartridge = (CheckBox)findViewById(R.id.checkBox);
        checkBoxQR = (CheckBox)findViewById(R.id.checkBox1);
        checkBoxConfig = (CheckBox)findViewById(R.id.checkBox2);

        checkBoxCartridge.setEnabled(false);
        checkBoxQR.setEnabled(false);
        checkBoxConfig.setEnabled(false);

        buttonStart = (Button)findViewById(R.id.button_startguide);
        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.

                new AlertDialog.Builder(GuideActivity.this)
                        .setTitle("VOC Monitor")
                        .setMessage("Check the cartridge.")
                        .setCancelable(false)
                        .setNeutralButton("Confirm", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //To change body of implemented methods use File | Settings | File Templates.
                                checkBoxCartridge.setEnabled(true);
                                checkBoxCartridge.setChecked(true);

                                buttonStart.setText("Scan QR");
                                buttonStart.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        //To change body of implemented methods use File | Settings | File Templates.
                                        new AlertDialog.Builder(GuideActivity.this)
                                                .setTitle("VOC Monitor")
                                                .setMessage("Scan the QR code of calibration factor.")
                                                .setCancelable(true)
                                                .setNeutralButton("Scan",
                                                        new DialogInterface.OnClickListener() {
                                                            //@Override
                                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                                //To change body of implemented methods use File | Settings | File Templates.
                                                                try {
                                                                    Intent myIntent = new Intent(
                                                                            getApplicationContext(),
                                                                            CaptureActivity.class);
                                                                    myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                                    startActivityForResult(
                                                                            myIntent, 13);
                                                                    //13);
                                                                } catch (Exception e) {
                                                                    e.printStackTrace();
                                                                }
                                                                checkBoxQR.setEnabled(true);
                                                                checkBoxQR.setChecked(true);

                                                                buttonStart.setText("Config");
                                                                buttonStart.setOnClickListener(new View.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(View view) {
                                                                        //To change body of implemented methods use File | Settings | File Templates.
                                                                        Intent intent_main = new Intent(getBaseContext(),
                                                                                ConfigActivity.class);
                                                                        startActivityForResult(intent_main, 0);
                                                                    }
                                                                });

                                                            }
                                                        })
                                                .show();

                                    }
                                });

                            }
                        })
                        .show();


            }
        });

    }
}