package userinterface;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.TextView;
import net.bluetoothviewer.R;

/**
 * Created with IntelliJ IDEA.
 * User: linyaoli
 * Date: 11/26/13
 * Time: 1:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class Welcome extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome);


        final Animation animAlpha = AnimationUtils.loadAnimation(this, R.anim.anim_translate);

        ImageButton buttonStart = (ImageButton)findViewById(R.id.button_start);
        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                //animation
                view.startAnimation(animAlpha);
                TextView tx = (TextView)findViewById(R.id.textView3);
                tx.startAnimation(animAlpha);

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        Intent intent_main = new Intent(getBaseContext(),
                                GuideActivity.class);
                        startActivityForResult(intent_main, 0);

                    }
                }, 300);
            }
        });
        ImageButton buttonHistory = (ImageButton)findViewById(R.id.button_history);
        buttonHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                view.startAnimation(animAlpha);
                TextView tx = (TextView)findViewById(R.id.textView4);
                tx.startAnimation(animAlpha);

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        Intent intent_main = new Intent(getBaseContext(),
                                HistoryActivity.class);
                        //SlidingDrawerActivity.class);
                        startActivityForResult(intent_main, 0);
                    }
                }, 300);
            }
        });
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
    }

}