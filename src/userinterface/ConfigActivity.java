package userinterface;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.*;
import net.bluetoothviewer.R;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: linyaoli
 * Date: 11/27/13
 * Time: 3:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class ConfigActivity extends Activity {

    private static View lastViewInScenario;
    private static View lastViewInMode;

    private static String stringScenario;
    private static String stringMode;

    private ArrayAdapter<String> mPairedDevicesArrayAdapter;
    private ArrayAdapter<String> mNewDevicesArrayAdapter;
    private ArrayAdapter<CheckBox> mScenarioCheckboxArrayAdapter;
    private ArrayAdapter<CheckBox> mModeCheckboxArrayAdapter;
    private Set<String> mNewDevicesSet;

    //Bundle transfer
    private static  final Bundle bData = new Bundle();

    private Button scanButton;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // Setup the window
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.config);

        // Set result CANCELED incase the user backs out
        setResult(Activity.RESULT_CANCELED);

        // Initialize the button to perform device discovery
        scanButton = (Button) findViewById(R.id.button_confirm);
        scanButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                bData.putString("scenario", stringScenario);
                bData.putString("mode", stringMode);

                Intent intent_main = new Intent(getBaseContext(),
                        BluetoothViewer.class);
                intent_main.putExtras(bData);
                startActivityForResult(intent_main, 0);

                //v.setVisibility(View.GONE);
            }
        });

        // Initialize array adapters. One for already paired devices and
        // one for newly discovered devices

        mPairedDevicesArrayAdapter = new ArrayAdapter<String>(this, R.layout.device_name);
        mNewDevicesArrayAdapter = new ArrayAdapter<String>(this, R.layout.device_name);
        mNewDevicesSet = new HashSet<String>();

        findViewById(R.id.title_scenario).setVisibility(View.VISIBLE);
        findViewById(R.id.title_mode).setVisibility(View.VISIBLE);


        mPairedDevicesArrayAdapter.add("Default");
        mPairedDevicesArrayAdapter.add("Industry");
        mPairedDevicesArrayAdapter.add("Indoor");
        mPairedDevicesArrayAdapter.add("Environment");

        mNewDevicesArrayAdapter.add("Singleton");
        mNewDevicesArrayAdapter.add("Continuous");
        // Find and set up the ListView for paired devices

        ListView scenarioListView = (ListView) findViewById(R.id.listview_scenario);

        scenarioListView.setAdapter(mPairedDevicesArrayAdapter);
        scenarioListView.setOnItemClickListener(mScenarioListener);


//        pairedListView.setOnItemClickListener(mDeviceClickListener);

        // Find and set up the ListView for newly discovered devices
        ListView modeListView = (ListView) findViewById(R.id.listview_mode);
        modeListView.setAdapter(mNewDevicesArrayAdapter);
        modeListView.setOnItemClickListener(mModeListener);

//        newDevicesListView.setOnItemClickListener(mDeviceClickListener);

    }

    private AdapterView.OnItemClickListener mScenarioListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {

            if (lastViewInScenario == null) {
                lastViewInScenario = v;
                v.setBackgroundColor(Color.rgb(47, 65, 49));
            }
            else {
                v.setBackgroundColor(Color.rgb(47, 65, 49));
                lastViewInScenario.setBackgroundColor(Color.rgb(0, 0, 0));
                lastViewInScenario = v;
            }
            stringScenario = ((TextView)v ).getText().toString();
            //String info = ((TextView) v).getText().toString();
            //String address = info.substring(info.length() - 17);

//            finish();
        }
    };

    private AdapterView.OnItemClickListener mModeListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {

            if (lastViewInMode == null) {
                lastViewInMode = v;
                v.setBackgroundColor(Color.rgb(47, 65, 49));
            }
            else {
                v.setBackgroundColor(Color.rgb(47, 65, 49));
                lastViewInMode.setBackgroundColor(Color.rgb(0, 0, 0));
                lastViewInMode = v;
            }

            stringMode = ((TextView)v ).getText().toString();
            //String info = ((TextView) v).getText().toString();
            //String address = info.substring(info.length() - 17);

//            finish();
        }
    };

    public String getMode()  {
        return stringMode;
    }

    public String getScenario()  {
        return stringScenario;
    }
}