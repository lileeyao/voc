package display;

import processing.SensorCurve;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.Queue;

/**
 * Created with IntelliJ IDEA.
 * User: linyaoli
 * Date: 11/19/13
 * Time: 3:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class DynamicPlotDataSource implements Runnable{

    private SensorCurve mSensorCurve;
    private Queue<Character> dataQueueBuffer;
    private short amp = 50;
    private static final String TAG = "EnviroSensor";

    /**
     * Constructor.
     * @param mSensorCurve : calculation of sensor values.
     * @param mode : select display of ppb mode or raw mode.
     */
    public DynamicPlotDataSource(SensorCurve mSensorCurve, String mode) {
        this.mSensorCurve = mSensorCurve;
        for (int i = 0; i< SAMPLE_SIZE; i++)
            ampArrayList.add((char)(0));

        if (mode.equals("ppb")) {
            isPpb = true;
            SAMPLE_SIZE = 11;
        }
        else if (mode.equals("raw")) {
            isRaw = true;
            SAMPLE_SIZE = 200;
        }
    }

    class MyObservable extends Observable {
        @Override
        public void notifyObservers () {
            setChanged();
            super.notifyObservers();
        }
    }
//    private static final int MAX_AMP_SEED = 100;
//    private static final int MIN_AMP_SEED = 10;
//    private static final int AMP_STEP = 5;
    private int phase = 0;
    //private int sinAmp = 100;
    private static boolean isPurging = false;

    public static final int SINE1 = 0;
    public static final int SINE2 = 1;
    private int SAMPLE_SIZE = 200;

    private MyObservable notifier;
    private ArrayList<Character> ampArrayList = new ArrayList<Character>();
    private ArrayList<Character> ppbArrayList = new ArrayList<Character>();
    private static int waitingTime = 1000;


    {
        notifier = new MyObservable();
    }

    private boolean isRaw = false;
    private boolean isPpb = false;

    /**
     * Running in infinite loop, keep refreshing buffers.
     *
     */

    public void run() {
        try {
            boolean isRising = true;

            while (true) {

                if (isRaw) setRaw();
                else if (isPpb) setPpb();
                // decrease or remove to speed up the refresh rate.
                Thread.sleep(waitingTime);
                phase++;
//                if (sinAmp >= MAX_AMP_SEED) {
//                    isRising = false;
//                } else if (sinAmp <= MIN_AMP_SEED) {
//                    isRising = true;
//                }
//
//                if (isRising) {
//                    sinAmp += AMP_STEP;
//                } else {
//                    sinAmp -= AMP_STEP;
//                }
//
                notifier.notifyObservers();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param series
     * @return
     */
    public int getItemCount(int series) {
        return SAMPLE_SIZE-1;
    }

    /**
     *
     * @param series
     * @param index
     * @return
     */
    public Number getX(int series, int index) {
        if (index >= SAMPLE_SIZE) {
            throw new IllegalArgumentException();
        }
        return index;
    }

    /**
     * Get the corresponding Y value by X.
     * @param series
     * @param index
     * @return
     */
    public Number getY(int series, int index) {

        if (index >= SAMPLE_SIZE) {
            throw new IllegalArgumentException();
        }
        //DecimalFormat df = new DecimalFormat("####");
        switch (series) {
            case SINE1:
                int k = 0;
                k = index;
                if (isRaw) return playRaw(k);
                else if (isPpb) return playPpb(k);
            case SINE2:
                //return -Short.parseShort(df.format(amp));
                return amp;
            default:
                throw new IllegalArgumentException();
        }
    }

    /**
     *
     * @param observer
     */
    public void addObserver(Observer observer) {
        notifier.addObserver(observer);
    }

    /**
     *
     * @param observer
     */
    public void removeObserver(Observer observer) {
        notifier.deleteObserver(observer);
    }

    /**
     *
     */
    private void setRaw() {
        dataQueueBuffer = mSensorCurve.syncData();
        while (dataQueueBuffer.size() > 0) {
            Character tmp = dataQueueBuffer.poll();
            ampArrayList.add(tmp);
            dataQueueBuffer = mSensorCurve.syncData();
        }
    }

    /**
     * Update raw data queue buffer, get value from buffer.
     * @param k
     * @return
     */
    private int playRaw(int k) {
        try {
            dataQueueBuffer = mSensorCurve.syncData();
            while (ampArrayList.size() >= SAMPLE_SIZE)
                ampArrayList.remove(0);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return Math.abs(ampArrayList.get(k));
    }

    /**
     *
     */
    private void setPpb() {
        ppbArrayList = mSensorCurve.getPpbArrayList();
    }

    /**
     * Update the ppb array, get the value from each socket.
     * @param k
     * @return
     */
    private int playPpb(int k) {

        try {
            ppbArrayList = mSensorCurve.getPpbArrayList();
            while (ppbArrayList.size() >= SAMPLE_SIZE)
                ppbArrayList.remove(0);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return Math.abs(ppbArrayList.get(k));

    }
}

