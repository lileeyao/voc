package display;

/**
 * Created with IntelliJ IDEA.
 * User: linyaoli
 * Date: 11/19/13
 * Time: 3:34 PM
 * To change this template use File | Settings | File Templates.
 */

import com.androidplot.xy.XYSeries;

public class DynamicPlotSeries implements XYSeries {
    private DynamicPlotDataSource datasource;
    private int seriesIndex;
    private String title;

    /**
     *
     * @param datasource
     * @param seriesIndex
     * @param title
     */
    public DynamicPlotSeries(DynamicPlotDataSource datasource, int seriesIndex, String title) {
        this.datasource = datasource;
        this.seriesIndex = seriesIndex;
        this.title = title;
    }

    /**
     *
     * @return
     */
    @Override
    public String getTitle() {
        return title;
    }

    /**
     *
     * @return
     */
    @Override
    public int size() {
        return datasource.getItemCount(seriesIndex);
    }

    /**
     *
     * @param index
     * @return
     */
    @Override
    public Number getX(int index) {
        return datasource.getX(seriesIndex, index);
    }

    /**
     *
     * @param index
     * @return
     */
    @Override
    public Number getY(int index) {
        return datasource.getY(seriesIndex, index);
    }
}