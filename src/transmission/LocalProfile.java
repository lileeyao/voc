package transmission;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.provider.BaseColumns;
import au.com.bytecode.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class LocalProfile extends SQLiteOpenHelper{
	
	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "FeedReader.db";
    private static SQLiteDatabase db;

    /* Inner class that defines the table contents */
    public static abstract class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "entry";
        public static final String COLUMN_NAME_ENTRY_ID = "entryid";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_SUBTITLE = "subtitle";

     //   ...
    }
	
	private static final String TEXT_TYPE = " TEXT";
	private static final String COMMA_SEP = ",";
	private static final String SQL_CREATE_ENTRIES =
	    "CREATE TABLE " + FeedEntry.TABLE_NAME + " (" +
	    FeedEntry._ID + " INTEGER PRIMARY KEY," +
	    FeedEntry.COLUMN_NAME_ENTRY_ID + TEXT_TYPE + COMMA_SEP +
	    FeedEntry.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
	    //... // Any other options for the CREATE command
	    " )";

	private static final String SQL_DELETE_ENTRIES =
	    "DROP TABLE IF EXISTS " + FeedEntry.TABLE_NAME;

    /**
     *
     * @param context
     */
	public LocalProfile(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);

		// TODO Auto-generated constructor stub
	}

    /**
     *
     * @param db
     */
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
        final String SQL = "CREATE TABLE IF NOT EXISTS " + "VOC" + "( " +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "_date DATETIME DEFAULT CURRENT_TIMESTAMP, " +
                "_VALUE1 VARCHAR(50)," +
                "_VALUE2 VARCHAR(50)," +
                "_VALUE3 VARCHAR(50)," +
                "_VALUE4 VARCHAR(50)," +
                "_VALUE5 VARCHAR(50)," +
                "_VALUE6 VARCHAR(50)," +
                "_VALUE7 VARCHAR(50)," +
                "_VALUE8 VARCHAR(50)," +
                "_VALUE9 VARCHAR(50)," +
                "_VALUE10 VARCHAR(50)" +
                //"_CONTENT TEXT," +
                //"_KIND VARCHAR(10)" +
                ");";
        final String SQL_QR = "CREATE TABLE IF NOT EXISTS " + "QR" + "( " +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "_date DATETIME DEFAULT CURRENT_TIMESTAMP, " +
                "_TITLE VARCHAR(50)," +
                "_CODE TEXT" +
                ");";

        final String SQL_CONC = "CREATE TABLE IF NOT EXISTS " + "CONC" + "( " +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "_date DATETIME DEFAULT CURRENT_TIMESTAMP, " +
                "_VALUE1 VARCHAR(50)," +
                "_VALUE2 VARCHAR(50)" +
                //"_CONTENT TEXT," +
                //"_KIND VARCHAR(10)" +
                ");";
        try {
            db.execSQL(SQL);
            db.execSQL(SQL_QR);
            db.execSQL(SQL_CONC);
           // db.close();
        }
        catch ( Exception e ) {
            e.printStackTrace();
        }
	}

    /**
     *
     * @param db
     * @param oldVersion
     * @param newVersion
     */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		// This database is only a cache for online data.
		db.execSQL(SQL_DELETE_ENTRIES);
		onCreate(db);
	}

    /**
     *
     * @param db
     * @param oldVersion
     * @param newVersion
     */
	@SuppressLint("Override")
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		onUpgrade(db, oldVersion, newVersion);
    }

    /**
     *
     */
    private void checkExternalMedia(){
        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWriteable = false;
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // Can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // Can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            // Can't read or write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }
    }

    /** Method to write ascii text characters to file on SD card. Note that you must add a
     WRITE_EXTERNAL_STORAGE permission to the manifest file or this method will throw
     a FileNotFound Exception because you won't have write permission. */
    /**
     *
     * @param path
     * @return
     */
    private File writeToSDFile(String path){

        // Find the root of the external storage.
        // See http://developer.android.com/guide/topics/data/data-  storage.html#filesExternal

        File root = android.os.Environment.getExternalStorageDirectory();
        // See http://stackoverflow.com/questions/3551821/android-write-to-sd-card-folder

        File dir = new File (root.getAbsolutePath() + "/monitor");
        dir.mkdirs();
        File file = new File(dir, path+".csv");
        return file;
    }

    /**
     *
     * @param date
     * @return
     */
    private File createFile (String date) {

        checkExternalMedia();
        return writeToSDFile(date);
    }

    /**
     *
     * @param date
     * @return
     * @throws IOException
     */
    public CSVWriter initializeCSV(String date) throws IOException {

        File file = createFile(date.substring(0, 15));
        CSVWriter writer = new CSVWriter(new FileWriter(file));
        return writer;
    }

    /**
     *
     * @param date
     * @return
     * @throws IOException
     */
    public CSVWriter initializeCSV_ppb(String date) throws IOException {
        File file = createFile(date.substring(0, 15) + "_concentration");
        CSVWriter writer = new CSVWriter(new FileWriter(file));
        return writer;
    }

    /**
     *
     * @param writer
     * @param line
     * @throws IOException
     */
    public static void saveCSVLine (CSVWriter writer, String[] line) throws IOException {
        writer.writeNext(line);
    }
//
////******************************** Some Comments *************************//
///*
// * To access your database, instantiate your subclass of SQLiteOpenHelper:
// * FeedReaderDbHelper mDbHelper = new FeedReaderDbHelper(getContext());
// *
// * put info in a db
// * // Gets the data repository in write mode
//	SQLiteDatabase db = mDbHelper.getWritableDatabase();
//
//	// Create a new map of values, where column names are the keys
//	ContentValues values = new ContentValues();
//	values.put(FeedEntry.COLUMN_NAME_ENTRY_ID, id);
//	values.put(FeedEntry.COLUMN_NAME_TITLE, title);
//	values.put(FeedEntry.COLUMN_NAME_CONTENT, content);
//
//	// Insert the new row, returning the primary key value of the new row
//	long newRowId;
//	newRowId = db.insert(
//	         FeedEntry.TABLE_NAME,
//	         FeedEntry.COLUMN_NAME_NULLABLE,
//	         values);
// *
// * read from db
// *
// * SQLiteDatabase db = mDbHelper.getReadableDatabase();
//
//	// Define a projection that specifies which columns from the database
//	// you will actually use after this query.
//	String[] projection = {
//	    FeedEntry._ID,
//	    FeedEntry.COLUMN_NAME_TITLE,
//	    FeedEntry.COLUMN_NAME_UPDATED,
//	    ...
//	    };
//
//	// How you want the results sorted in the resulting Cursor
//	String sortOrder =
//	    FeedEntry.COLUMN_NAME_UPDATED + " DESC";
//
//	Cursor c = db.query(
//	    FeedEntry.TABLE_NAME,  // The table to query
//	    projection,                               // The columns to return
//	    selection,                                // The columns for the WHERE clause
//	    selectionArgs,                            // The values for the WHERE clause
//	    null,                                     // don't group the rows
//	    null,                                     // don't filter by row groups
//	    sortOrder                                 // The sort order
//	    );
// */
	
}
