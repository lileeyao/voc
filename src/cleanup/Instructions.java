package cleanup;

/**
 * Created with IntelliJ IDEA.
 * User: linyaoli
 * Date: 11/25/13
 * Time: 5:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class Instructions {

    private static final String CHECK_CARTRIDGE = "Insert cartridge.";
    private static final String CHECK_CARTRIDGE_SUFFIX = "Before setup, make sure your cartridge is ready.";
    private static final String QR_SCAN = "QR code.";
    private static final String QR_SCAN_SUFFIX = "Scan the QR code to setup the calibration factor.";
    private static final String CONFIG = "Running configuration.";
    private static final String CONFIG_SUFFIX = "";

    /**
     *
     * @param referName
     * @return
     */
    public String getInstruction (String referName) {

        return referName;
    }


}
