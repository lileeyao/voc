package cleanup;


import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.TextView;

/**
 *
 *
 */
public class DrawGraph {
	
	private ThreadPD myThreadPd1, myThreadPd2, myThreadPd3;
	private SurfaceView view1, view2, view3;
	private TextView text1,text2,text3;
	private static boolean colorChanger;

    /**
     *
     */
	public void runGraph () {
		SurfaceHolder holder1 = view1.getHolder();
		holder1.addCallback(mSurfaceCallbackPd1);
		//holder1.lockCanvas().drawColor(Color.GRAY);
		this.myThreadPd1 = new ThreadPD(holder1, 2);
		
		SurfaceHolder holder2 = view2.getHolder();
		holder2.addCallback(mSurfaceCallbackPd2);
		this.myThreadPd2 = new ThreadPD(holder2, 3);
		
		SurfaceHolder holder3 = view3.getHolder();
		holder3.addCallback(mSurfaceCallbackPd3);		
		this.myThreadPd3 = new ThreadPD(holder3, 4);
		
	}

    /**
     *
     * @param view1
     * @param view2
     * @param view3
     */
	public DrawGraph(SurfaceView view1, 
			SurfaceView view2,
			SurfaceView view3) {
		this.view1 = view1;
		this.view2 = view2;
		this.view3 = view3;
		colorChanger = true;
		
	}

    /**
     *
     */
	class ThreadPD extends Thread {
		private SurfaceHolder holder;
		public boolean isRun;

        /**
         *
         * @param holder
         * @param colorChangerX
         */
		public ThreadPD(SurfaceHolder holder, int colorChangerX) {
			this.holder = holder;
			isRun = true;			
		}

        /**
         *
         */
		@Override
		public void run() {
			float count = 0;
			
			while (isRun) {
				Canvas canvas = null;
				try {
					synchronized (holder) {						
						canvas = holder.lockCanvas(); // Draw on surface view after the object is locked.
						if (colorChanger) {
							//canvas.drawColor(Color.WHITE);
							colorChanger = false;
						}
						Paint paint = new Paint();
						paint.setColor(Color.RED);
						paint.setAntiAlias(true);
						paint.setTextSize(30);
						paint.setStrokeWidth(2);

						
						paint.setColor(Color.GREEN);
						canvas.drawLine(0, 95, 400, 95, paint);
						for (int i = 0; i < 400; i++)
							if (i % 10 == 0)
								canvas.drawLine(i, 95, i, 90, paint);
						canvas.drawLine(2, 110, 2, 0, paint);
						for (int i = 0; i < 80; i++)
							if (i % 10 == 0)
								canvas.drawLine(2, i, 7, i, paint);
						
						paint.setColor(Color.RED);
						
						count += 0.1;
						
						//canvas.drawPoint(count+(float)0.1, (float)Math.sin(count)*100, paint);
						if (count*5 < 400) {
							canvas.drawLine(count*5, Math.abs((float)Math.sin(count)*100), (float) ((count+0.1)*5), Math.abs((float)Math.sin(count+0.1)*100), paint);
						}	
						else {							
							//canvas.drawColor(Color.WHITE);
							count = 0;
						}
					//	text1.setText(Float.toString(count));
					//	text2.setText(Float.toString(count));
					//	text3.setText(Float.toString(count));
					
						
						//canvas.drawText((count++)+" seconds", 20, 40, paint);
						Thread.sleep(10);
						
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}
				finally {
					if (canvas != null) {
						holder.unlockCanvasAndPost(canvas);//unlock view, update change.
					}
				}
			}
		}
	}

    /**
     *
     */
	private SurfaceHolder.Callback mSurfaceCallbackPd1 = 
			new SurfaceHolder.Callback() {
				
				@Override
				public void surfaceDestroyed(SurfaceHolder holder) {
					// TODO Auto-generated method stub
					myThreadPd1.isRun = false;
				}
				
				@Override
				public void surfaceCreated(SurfaceHolder holder) {
					// TODO Auto-generated method stub
					myThreadPd1.isRun = true;
					myThreadPd1.start();
				}
				
				@Override
				public void surfaceChanged(SurfaceHolder holder, int format, int width,
						int height) {
					// TODO Auto-generated method stub
					
				}
			};
    /**
     *
     */
	private SurfaceHolder.Callback mSurfaceCallbackPd2 = 
			new SurfaceHolder.Callback() {
				
				@Override
				public void surfaceDestroyed(SurfaceHolder holder) {
					// TODO Auto-generated method stub
					myThreadPd2.isRun = false;
				}
				
				@Override
				public void surfaceCreated(SurfaceHolder holder) {
					// TODO Auto-generated method stub
					myThreadPd2.isRun = true;
					myThreadPd2.start();
				}
				
				@Override
				public void surfaceChanged(SurfaceHolder holder, int format, int width,
						int height) {
					// TODO Auto-generated method stub
					
				}
			};
	private SurfaceHolder.Callback mSurfaceCallbackPd3 = 
				new SurfaceHolder.Callback() {
					
				@Override
				public void surfaceDestroyed(SurfaceHolder holder) {
					// TODO Auto-generated method stub
					myThreadPd3.isRun = false;
				}
				
				@Override
				public void surfaceCreated(SurfaceHolder holder) {
					// TODO Auto-generated method stub
					myThreadPd3.isRun = true;
					myThreadPd3.start();
				}
				
				@Override
				public void surfaceChanged(SurfaceHolder holder, int format, int width,
						int height) {
					// TODO Auto-generated method stub
					
				}
			};
}
