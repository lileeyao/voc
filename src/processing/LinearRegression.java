package processing;
/**
 * Created with IntelliJ IDEA.
 * User: linyaoli
 * Date: 11/19/13
 * Time: 3:31 PM
 * Linear regression, find the line from sampling points.
 */

import java.util.Queue;

public class LinearRegression{

    private static int MAXN = 1000;
    private double[] x = new double[30];
    private double[] y = new double[30];
    private LinearParams returnedLinearParams = new LinearParams();

    /**
     * Constructor.
     */
    public class LinearParams {
        public double beta0 = 0;
        public double beta1 = 0;
    }
    /*
    /**
     *
     * @param qfCount30
     * @return

    public LinearParams getLinearParams(Queue<Float> qfCount30) {

//        return lineFit(qfCount30.toArray());
        return null;
    }
    */

    /**
     * Return the line parameters, coefficient (y=ax+b) for linear function.
     * @param arr
     * @return
     */
    public LinearParams lineFit (char[] arr) {

        // first pass: read in data, compute xbar and ybar
        int n = arr.length;
        for (int i=0;i<n;i++){
            x[i] = i;
            y[i] = (double)arr[i];
        }
        estimate(x, y, n );
        return returnedLinearParams;

    }


    /**
     * linear regression
     * y = a x + b
     * b = sum( y ) / n - a * sum( x ) / n
     * a = ( n * sum( xy ) - sum( x ) * sum( y ) ) / ( n * sum( x^2 ) - sum(x) ^ 2 )
     *
     */

    /**
     * prediction.
     * @param x
     * @param y
     * @param i
     * @return
     */
    private void estimate( double[] x , double[] y , int i ) {
        double a = getXc( x , y ) ;
        double b = getC( x , y , a ) ;
        returnedLinearParams.beta0 = a;
        returnedLinearParams.beta1 = b;
    }

    /**
     * get coeffecient of x
     * @param x
     * @param y
     * @return
     */
    public static double getXc( double[] x , double[] y ){
        int n = x.length ;
        return ( n * pSum( x , y ) - sum( x ) * sum( y ) )
                / ( n * sqSum( x ) - Math.pow(sum(x), 2) ) ;
    }
    /**
     * @param x
     * @param y
     * @param a
     * @return
     */
    public static double getC( double[] x , double[] y , double a ){
        int n = x.length ;
        return sum( y ) / n - a * sum( x ) / n ;
    }

    /**
     * constant
     * @param x
     * @param y
     * @return
     */
    public static double getC( double[] x , double[] y ){
        int n = x.length ;
        double a = getXc( x , y ) ;
        return sum( y ) / n - a * sum( x ) / n ;
    }

    /**
     *
     * @param ds
     * @return
     */
    private static double sum(double[] ds) {
        double s = 0 ;
        for( double d : ds ) s = s + d ;
        return s ;
    }

    /**
     *
     * @param ds
     * @return
     */
    private static double sqSum(double[] ds) {
        double s = 0 ;
        for( double d : ds ) s = s + Math.pow(d, 2) ;
        return s ;
    }

    /**
     *
     * @param x
     * @param y
     * @return
     */
    private static double pSum( double[] x , double[] y ) {
        double s = 0 ;
        for( int i = 0 ; i < x.length ; i++ ) s = s + x[i] * y[i] ;
        return s ;
    }
}
