VOC Monitor Tool
===============
This app is a lab project used for monitoring chemical sensor reading.
Functionalities include Bluetooth connection(2.1 & BLE), visual plotting based on AndroidPlot and 
multiple side functions such as databases and QR encoding.

Limitations
-----------
1. Potential data loss in bluetooth transmission, depends on the device.

Feature ideas
-------------
![Alt text](1.png)

|    No.    |  features  |
|-----------|------------|
|    1      |non-blocking Real-time data plotting.|
|2|Bluetooth 2.1 transmission (allow r/c, pause and resume).|
|3|QR code scan.|
|4|History review, log rotation in SQLite.|

Contributing code
-----------------
Free to use and contribute.

Donations
---------


Disclaimer
----------
This app starts from the original project of BluetoothViewer.
refer to https://github.com/janosgyerik/bluetoothviewer
